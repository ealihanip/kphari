@extends('layouts.default')

@section('title', 'Santri')

@section('content')
    
    <div class='row'>
        <div class='col'>
            <a class="btn btn-primary" href="{{ route('santri.create') }}" role="button">Tambah Data</a>
        </div>
        
    </div>
    <br>
    <div class='row'>
        <div class='col'>
            @include('santri.datatable')
        </div>
        
    </div>
    
@endsection