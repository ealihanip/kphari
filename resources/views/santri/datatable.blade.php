

<table class="table table-bordered" id="tabel-santri">
    <thead>
        <tr>
            <th>NIS</th>
            <th>Angkatan</th>
            <th>Nama</th>
            <th>Action</th>
        </tr>
    </thead>
</table>

<script>
    $(document).ready( function () {
        
        $('#tabel-santri').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('santri.getdata') }}',
            columns: [
                {data:'nis'},
                {data:'angkatan'},
                {data:'nama_santri'},
                {data:'action', orderable: false, searchable: false}
            ]
        });
    });
</script>

