

@extends('layouts.default')

@section('title', 'Data Santri')

@section('content')
    <div class='row'>
        
        <div class='col-sm-12'>
    
            <a href="{{ route('santri.index') }}" class="btn btn-primary">Kembali</a>
        
        </div>

        <!-- data -->
        <div class='col-sm-2'>
            <p class='text-right'>NIS :</p>
        </div>

        <div class='col-sm-10'>
            <p>{{ $santri->nis }}</p>
        </div>
        <!-- end data -->

        <!-- data -->
        <div class='col-sm-2'>
            <p class='text-right'>Angkatan :</p>
        </div>

        <div class='col-sm-10'>
            <p>{{ $santri->angkatan }}</p>
        </div>
        <!-- end data -->

        <!-- data -->
        <div class='col-sm-2'>
            <p class='text-right'>Nama Lengkap :</p>
        </div>

        <div class='col-sm-10'>
            <p>{{ $santri->nama_santri }}</p>
        </div>
        <!-- end data -->

        <!-- data -->
        <div class='col-sm-2'>
            <p class='text-right'>Jenis Kelamin :</p>
        </div>

        <div class='col-sm-10'>
            <p>{{ $santri->jenis_kelamin }}</p>
        </div>
        <!-- end data -->

        <!-- data -->
        <div class='col-sm-2'>
            <p class='text-right'>Tempat Lahir :</p>
        </div>

        <div class='col-sm-10'>
            <p>{{ $santri->tempat_lahir }}</p>
        </div>
        <!-- end data -->

        <!-- data -->
        <div class='col-sm-2'>
            <p class='text-right'>Tanggal Lahir :</p>
        </div>

        <div class='col-sm-10'>
            <p>{{ $santri->tanggal_lahir }}</p>
        </div>
        <!-- end data -->

        <!-- data -->
        <div class='col-sm-2'>
            <p class='text-right'>Alamat :</p>
        </div>

        <div class='col-sm-10'>
            <p>{{ $santri->alamat }}</p>
        </div>
        <!-- end data -->
        
        <!-- data -->
        <div class='col-sm-2'>
            <p class='text-right'>Nama Orang Tua :</p>
        </div>

        <div class='col-sm-10'>
            <p>{{ $santri->nama_orang_tua }}</p>
        </div>
        <!-- end data -->

        <!-- data -->
        <div class='col-sm-2'>
            <p class='text-right'>No Hp Orang Tua :</p>
        </div>

        <div class='col-sm-10'>
            <p>{{ $santri->no_hp_orang_tua }}</p>
        </div>
        <!-- end data -->

        <!-- data -->
        <div class='col-sm-2'>
            <p class='text-right'>Foto</p>
        </div>

        <div class='col-sm-10'>
            <img src='{{url($santri->foto)}}' width='300px' height='auto'>
        </div>
        <!-- end data -->
        
    </div>

@endsection