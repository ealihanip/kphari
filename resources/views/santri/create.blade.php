


@extends('layouts.default')

@section('title', 'Tambah Data Santri')

@section('content')
   
    
    {{-- part alert --}}
    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-dismissible alert-{{ Session::get('message.alert') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ Session::get('message.title') }}</strong>
                    
                </div>
            </div>
        </div>
    @endif
    
    <form action="{{ route('santri.store') }}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <!-- field -->
        <div class="form-group {{ $errors->has('nis') ? 'has-error' : '' }}">
            <label for="nis" class="control-label">NIS</label>
            <input type="text" class="form-control" name="nis" placeholder="NIS">
            @if ($errors->has('nis'))
                <span class="help-block text-danger">{{ $errors->first('nis') }}</span>
            @endif
        </div>
        <!-- end field -->
        
        <!-- field -->
        <div class="form-group {{ $errors->has('angkatan') ? 'has-error' : '' }}">
            <label for="angkatan">Angkatan</label>
				{{Form::select
                
                    (
                        'angkatan', 
                        array(
                            '2016' => '2016',
                            '2017' => '2017',
                            '2018' => '2018',
                            '2019' => '2018'
                            
                        ),"",
                        ['class' => 'form-control']
                        
                    )

				}}
				
            @if ($errors->has('angkatan'))
                <span class="help-block text-danger">{{ $errors->first('angkatan') }}</span>
            @endif
        </div>
        <!-- end field -->

        <!-- field -->
        <div class="form-group {{ $errors->has('nama_santri') ? 'has-error' : '' }}">
            <label for="nama_santri" class="control-label">Nama</label>
            <input type="text" class="form-control" name="nama_santri" placeholder="Nama">
            @if ($errors->has('nama_santri'))
                <span class="help-block text-danger">{{ $errors->first('nama_santri') }}</span>
            @endif
        </div>
        <!-- end field -->

        <!-- field -->
        <div class="form-group {{ $errors->has('jenis_kelamin') ? 'has-error' : '' }}">
            <label for="jenis_kelamin">Jenis Kelamin</label>
				{{Form::select
                
                    (
                        'jenis_kelamin', 
                        array(
                            'pria' => 'Pria',
                            'wanita' => 'Wanita'
                            
                        ),"",
                        ['class' => 'form-control']
                        
                    )

				}}
				
            @if ($errors->has('jenis_kelamin'))
                <span class="help-block text-danger">{{ $errors->first('jenis_kelamin') }}</span>
            @endif
        </div>
        <!-- end field -->

        <!-- field -->
        <div class="form-group {{ $errors->has('tempat_lahir') ? 'has-error' : '' }}">
            <label for="tempat_lahir" class="control-label">Tempat Lahir</label>
            <input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat Lahir">
            @if ($errors->has('tempat_lahir'))
                <span class="help-block text-danger">{{ $errors->first('tempat_lahir') }}</span>
            @endif
        </div>
        <!-- end field -->

        <!-- field -->
        <div class="form-group {{ $errors->has('tanggal_lahir') ? 'has-error' : '' }}">
            <label for="tanggal_lahir" class="control-label">Tanggal Lahir</label>
            <input type="text" class="form-control datepicker" name="tanggal_lahir" placeholder="Tanggal Lahir">
            @if ($errors->has('tanggal_lahir'))
                <span class="help-block text-danger">{{ $errors->first('tanggal_lahir') }}</span>
            @endif
        </div>
        <!-- end field -->

        <!-- field -->
        <div class="form-group {{ $errors->has('alamat') ? 'has-error' : '' }}">
            <label for="alamat" class="control-label">Alamat</label>
            <input type="text" class="form-control" name="alamat" placeholder="Alamat">
            @if ($errors->has('alamat'))
                <span class="help-block text-danger">{{ $errors->first('alamat') }}</span>
            @endif
        </div>
        <!-- end field -->

        <!-- field -->
        <div class="form-group {{ $errors->has('nama_orang_tua') ? 'has-error' : '' }}">
            <label for="nama_orang_tua" class="control-label">Nama Orang Tua</label>
            <input type="text" class="form-control" name="nama_orang_tua" placeholder="Nama Orang Tua">
            @if ($errors->has('nama_orang_tua'))
                <span class="help-block text-danger">{{ $errors->first('nama_orang_tua') }}</span>
            @endif
        </div>
        <!-- end field -->

        <!-- field -->
        <div class="form-group {{ $errors->has('no_hp_orang_tua') ? 'has-error' : '' }}">
            <label for="no_hp_orang_tua" class="control-label">No Hp Orang Tua</label>
            <input type="text" class="form-control" name="no_hp_orang_tua" placeholder="No Hp Orang Tua">
            @if ($errors->has('no_hp_orang_tua'))
                <span class="help-block text-danger">{{ $errors->first('no_hp_orang_tua') }}</span>
            @endif
        </div>
        <!-- end field -->

        <div class="form-group {{ $errors->has('no_hp_orang_tua') ? 'has-error' : '' }}">
            <label for="foto">Foto</label>
            <div class="custom-file">
                <input class="custom-file-input" id="foto" type="file" name="foto">
                <label class="custom-file-label" for="foto">Foto</label>
            </div>

            @if ($errors->has('foto'))
                <span class="help-block text-danger">{{ $errors->first('foto') }}</span>
            @endif
        </div>
        
        
        
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="{{ route('santri.index') }}" class="btn btn-default">Kembali</a>
        </div>
    </form>

@endsection