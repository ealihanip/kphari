@extends('layouts.default')

@section('title', 'Setting Tagihan')

@section('content')
    
    <div class='row'>
        <div class='col'>
            <a class="btn btn-primary" href="{{ route('settingtagihan.create') }}" role="button">Tambah Data</a>
        </div>
        
    </div>
    <br>
    <div class='row'>
        <div class='col'>
            @include('settingtagihan.datatable')
        </div>
        
    </div>
    
@endsection