

<table class="table table-bordered" id="tabel-settingtagihan">
    <thead>
        <tr>
            <th>Nama Tagihan</th>
            <th>Nominal</th>
            <th>Action</th>
        </tr>
    </thead>
</table>

<script>
    $(document).ready( function () {
        
        $('#tabel-settingtagihan').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('settingtagihan.getdata') }}',
            columns: [
                {data:'nama_tagihan'},
                {data:'nominal'},
                {data:'action', orderable: false, searchable: false}
            ]
        });
    });
</script>

