


@extends('layouts.default')

@section('title', 'Tambah Data Setting Tagihan')

@section('content')
   
    
    {{-- part alert --}}
    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-dismissible alert-{{ Session::get('message.alert') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ Session::get('message.title') }}</strong>
                    
                </div>
            </div>
        </div>
    @endif
    
    <form action="{{ route('settingtagihan.store') }}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <!-- field -->
        <div class="form-group {{ $errors->has('namatagihan') ? 'has-error' : '' }}">
            <label for="namatagihan" class="control-label">Nama Tagihan</label>
            <input type="text" class="form-control" name="namatagihan" placeholder="Nama Tagihan">
            @if ($errors->has('namatagihan'))
                <span class="help-block text-danger">{{ $errors->first('namatagihan') }}</span>
            @endif
        </div>
        <!-- end field -->

        <!-- field -->
        <div class="form-group {{ $errors->has('nominal') ? 'has-error' : '' }}">
            <label for="nominal" class="control-label">Nominal</label>
            <input type="text" class="form-control" name="nominal" placeholder="Nominal">
            @if ($errors->has('nominal'))
                <span class="help-block text-danger">{{ $errors->first('nominal') }}</span>
            @endif
        </div>
        <!-- end field -->
        
               
        
        
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="{{ route('settingtagihan.index') }}" class="btn btn-default">Kembali</a>
        </div>
    </form>

@endsection