


@extends('layouts.default')

@section('title', 'Buat Tagihan')

@section('content')
   
    
    {{-- part alert --}}
    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-dismissible alert-{{ Session::get('message.alert') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ Session::get('message.title') }}</strong>
                    
                </div>
            </div>
        </div>
    @endif
    
    <form action="{{ route('tagihan.store') }}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}

        <!-- field -->
        <div class="form-group {{ $errors->has('bulan') ? 'has-error' : '' }}">
            <label for="bulan">Bulan</label>
				{{Form::select
                
                    (
                        'bulan', 
                        array(
                            '1' => 'Januari',
                            '2' => 'February',
                            '3' => 'Maret',
                            '4' => 'April',
                            '5' => 'Mei',
                            '6' => 'Juni',
                            '7' => 'Juli',
                            '8' => 'Agustus',
                            '9' => 'September',
                            '10' => 'Oktober',
                            '11' => 'November',
                            '12' => 'Desember'
                            
                        ),"",
                        ['class' => 'form-control']
                        
                    )

				}}
				
            @if ($errors->has('bulan'))
                <span class="help-block text-danger">{{ $errors->first('bulan') }}</span>
            @endif
        </div>
        <!-- end field -->

        <!-- field -->
        <div class="form-group {{ $errors->has('tahun') ? 'has-error' : '' }}">
            <label for="tahun">Tahun</label>
				{{Form::select
                
                    (
                        'tahun', 
                        array(
                            '2018' => '2018',
                            '2019' => '2019',
                            '2020' => '2020'
                            
                        ),"",
                        ['class' => 'form-control']
                        
                    )

				}}
				
            @if ($errors->has('tahun'))
                <span class="help-block text-danger">{{ $errors->first('tahun') }}</span>
            @endif
        </div>
        <!-- end field -->
        
        
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="{{ route('tagihan.index') }}" class="btn btn-default">Kembali</a>
        </div>
    </form>

@endsection