@extends('layouts.default')

@section('title', 'Tagihan')

@section('content')
    
    <div class='row'>
        <div class='col'>
            <a class="btn btn-primary" href="{{ route('tagihan.create') }}" role="button">Tambah Data</a>
        </div>
        
    </div>
    <br>
    <div class='row'>
        <div class='col'>
            @include('tagihan.datatable')
        </div>
        
    </div>
    
@endsection