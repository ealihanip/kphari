

<table class="table table-bordered" id="tabel-detail-tagihan">
    <thead>
        <tr>
            <th>Nama Tagihan</th>
            <th>Nominal</th>
            
        </tr>
    </thead>
    <tbody>
    @foreach ($tagihan->detailtagihan as $dtagihan)
        <tr>
            <td>
                {{ $dtagihan->nama_tagihan }}
            </td>

            <td>
                {{ $dtagihan->nominal }}
            </td>
        
        </tr>
    @endforeach
    </tbody>
</table>
@push('scripts')
<script>
    $(document).ready( function (){
        
        $('#tabel-detail-tagihan').DataTable({
            
        });
    });
</script>
@endpush

