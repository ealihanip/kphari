<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      
        <span class="brand-text font-weight-light">Aplikasi Tagihan</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
         
          <li class="nav-item">
            <a href="{{ route('santri.index') }}" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Santri
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('tagihan.index') }}" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Tagihan
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('pembayaran.index') }}" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Pembayaran
                
              </p>
            </a>
          </li>
          
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>
                Pengaturan
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('settingtagihan.index') }}" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Setting Tagihan</p>
                </a>
              </li>
              
            </ul>
          </li>

          <li class="nav-item">
            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Logout
                
              </p>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
              </form>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  