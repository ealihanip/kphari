

@extends('layouts.default')

@section('title', 'Detail Tagihan')

@section('content')
    <div class='row'>
        
        <!-- data -->
        <div class='col-sm-12'>
    
            <a href="{{ route('tagihan.index') }}" class="btn btn-primary">Kembali</a>
        
        </div>
        <!-- end data -->

        <!-- data -->
        <div class='col-sm-12'>
            <h3 class='text-left'>Data Tagihan :</h3>
        </div>
        <!-- end data -->
        
        <!-- data -->
        <div class='col-sm-2'>
            <p class='text-right'>No Tagihan :</p>
        </div>

        <div class='col-sm-10'>
            <p>{{ $tagihan->no_tagihan }}</p>
        </div>
        <!-- end data -->

        <!-- data -->
        <div class='col-sm-2'>
            <p class='text-right'>Bulan :</p>
        </div>

        <div class='col-sm-10'>
            <p>{{ $tagihan->bulan }}</p>
        </div>
        <!-- end data -->

        <!-- data -->
        <div class='col-sm-2'>
            <p class='text-right'>Tahun :</p>
        </div>

        <div class='col-sm-10'>
            <p>{{ $tagihan->tahun }}</p>
        </div>
        <!-- end data -->

        <!-- data -->
        <div class='col-sm-2'>
            <p class='text-right'>NIS :</p>
        </div>

        <div class='col-sm-10'>
            <p>{{ $tagihan->santri->nis }}</p>
        </div>
        <!-- end data -->

        <!-- data -->
        <div class='col-sm-2'>
            <p class='text-right'>Nama Santri :</p>
        </div>

        <div class='col-sm-10'>
            <p>{{ $tagihan->santri->nama_santri }}</p>
        </div>
        <!-- end data -->

        <!-- data -->
        <div class='col-sm-2'>
            <p class='text-right'>Status :</p>
        </div>

        <div class='col-sm-10'>
            <p>{{ $tagihan->status }}</p>
        </div>
        <!-- end data -->

        <!-- data -->
        <div class='col-sm-12'>
            <h3 class='text-left'>List Tagihan :</h3>
        </div>
        <!-- end data -->


        <!-- data -->
        <div class='col-sm-12'>

            @include('tagihan.tabeldetailtagihan')

        </div>
        <!-- end data -->

        <!-- data -->
        <div class='col-sm-12'>
            <h3 class='text-left'>List Pembayaran :</h3>
        </div>
        <!-- end data -->


        <!-- data -->
        <div class='col-sm-12'>

            @include('tagihan.tabeldetailtagihan')

        </div>
        <!-- end data -->


        

    </div>

@endsection

