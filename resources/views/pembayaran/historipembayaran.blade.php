@extends('layouts.default')

@section('title', 'Pembayaran')

@section('content')

    {{-- part alert --}}
    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-dismissible alert-{{ Session::get('message.alert') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ Session::get('message.title') }}</strong>
                    
                </div>
            </div>
        </div>
    @endif
    
    <div class='row'>
        <div class='col'>
            @include('pembayaran.datatablepembayaran')
        </div>
        
    </div>
    
@endsection