


@extends('layouts.default')

@section('title', 'Bayar Tagihan')

@section('content')
   
    
    {{-- part alert --}}
    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-dismissible alert-{{ Session::get('message.alert') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ Session::get('message.title') }}</strong>
                    
                </div>
            </div>
        </div>
    @endif
    
    <form action="{{ route('pembayaran.store') }}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}

        <input type='hidden' name="tagihan_id" value="{{ $tagihanid }}">
        @foreach ($detailtagihan as $dtagihan)
            <!-- field -->
            <div class="form-group {{ $errors->has($dtagihan['nama_tagihan']) ? 'has-error' : '' }}">
                <label class="control-label">Nama: {{ $dtagihan['nama_tagihan'] }}</label>
                <br>
                <label class="control-label">Nominal: {{ $dtagihan['sisa'] }}</label>
                <input type="text" class="form-control" name="{{ $dtagihan['nama_tagihan'] }}" placeholder="Jumlah Bayar">
            </div>
            <!-- end field -->

            @if ($errors->has($dtagihan['nama_tagihan']))
                <span class="help-block text-danger">{{ $errors->first($dtagihan['nama_tagihan']) }}</span>
            @endif
        @endforeach

        
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Bayar</button>
            <a href="{{ route('pembayaran.index') }}" class="btn btn-default">Batal</a>
        </div>
    </form>

@endsection