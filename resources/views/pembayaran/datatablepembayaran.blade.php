

<table class="table table-bordered" id="tabel-pembayaran">
    <thead>
        <tr>
            <th>No Pembayaran</th>
            <th>Tanggal Pembayaran</th>
            <th>NIS</th>
            <th>Nama</th>
            <th>No Tagihan</th>
            <th>Status Tagihan</th>
            <th>Action</th>
        </tr>
    </thead>
</table>
@push('scripts')
<script>
    $(document).ready( function (){
        
        $('#tabel-pembayaran').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{route('pembayaran.getdatapembayaran')}}',
            columns: [
                {data:'no_pembayaran'},
                {data:'tanggal_pembayaran'},
                {data:'nis'},
                {data:'nama_santri'},
                {data:'no_tagihan'},
                {data:'status'},
                {data:'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
@endpush

