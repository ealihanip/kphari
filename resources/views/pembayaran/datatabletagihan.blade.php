

<table class="table table-bordered" id="tabel-tagihan">
    <thead>
        <tr>
            <th>No Tagihan</th>
            <th>NIS</th>
            <th>Nama</th>
            <th>Bulan</th>
            <th>Tahun</th>
            <th>Action</th>
        </tr>
    </thead>
</table>
@push('scripts')
<script>
    $(document).ready( function (){
        
        $('#tabel-tagihan').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{route('pembayaran.getdatatagihan')}}',
            columns: [
                {data:'no_tagihan'},
                {data:'santri.nis'},
                {data:'santri.nama_santri'},
                {data:'bulan'},
                {data:'tahun'},
                {data:'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
@endpush

