

@extends('layouts.default')

@section('title', 'Guru')

@section('content')

    <h4>Edit Guru</h4>
    <form action="{{ route('guru.update') }}" method="post">
        {{csrf_field()}}
        <input type="hidden" name="id" value="{{ $guru->id_guru }}">
        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
            <label for="title" class="control-label">Nama</label>
            <input type="text" class="form-control" name="nama" placeholder="Nama Guru" value="{{ $guru->nama_guru }}">
            @if ($errors->has('nama'))
                <span class="help-block">{{ $errors->first('nama') }}</span>
            @endif
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="{{ route('guru.index') }}" class="btn btn-default">Kembali</a>
        </div>
    </form>

    {{-- part alert --}}
    @if (Session::has('after_save'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-dismissible alert-{{ Session::get('after_save.alert') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ Session::get('after_save.title') }}</strong>
                    
                </div>
            </div>
        </div>
    @endif
    

    

@endsection