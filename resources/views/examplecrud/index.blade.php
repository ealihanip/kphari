@extends('layouts.default')

@section('title', 'Guru')

@section('content')
    
    <div class='row'>
        <div class='col'>
            <a class="btn btn-primary" href="{{ route('guru.create') }}" role="button">Tambah Data</a>
        </div>
        
        
    </div>
    <br>
    <div class='row'>
        <div class='col'>
            @include('guru.datatable')
        </div>
        
    </div>
    
@endsection