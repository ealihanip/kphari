

<table class="table table-bordered" id="tabel-guru">
    <thead>
        <tr>
            <th>ID</th>
            <th>Nama</th>
            <th>Action</th>
            
        </tr>
    </thead>
</table>

<script>
$(document).ready( function () {
    $('#tabel-guru').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('guru.getdata') }}',
        columns: [
            {data:'id_guru'},
            {data:'nama_guru'},
            {data:'action', orderable: false, searchable: false}
        ]
    });
});

</script>

