<?php

namespace App\Http\Controllers;
use Validator;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Models\Pembayaran;
use App\Models\Tagihan;
use App\Models\Settingtagihan;
use App\Models\Detailtagihan;
use App\Http\Requests\TagihanRequest;


class PembayaranController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('pembayaran.index');
    }

    public function historipembayaran()
    {

        return view('pembayaran.historipembayaran');
    }

    public function getdatatagihan()
    {
        $data = Tagihan::with(['santri'])->where('status', 'Belum Lunas');

        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '
                <a href="pembayaran/create/'.$data->id.'" class="btn btn-xs btn-primary"><i ></i> Bayar</a>
                ';
            })
            ->make(true);
    }

    public function getdatapembayaran()
    {
        $data = Pembayaran::join('tagihan', 'tagihan.id', '=', 'pembayaran.tagihan_id')
        ->join('santri', 'tagihan.santri_id', '=', 'santri.id')
        ->get();




        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '
                <a href="pembayaran/create/'.$data->id.'" class="btn btn-xs btn-primary"><i ></i> Bayar</a>
                ';
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        
        $data=$this->getDataDetailTagihan($id);
        return view('pembayaran.create',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $id=$request->tagihan_id;
        

        $messages = [
            'required' => 'Silahkan Masukan Nominal',
            'min'=>'error',
            'max'=>'Jumlah Lebih Besar Dari Sisa Tagihan'
            
        ];

        $validationconfig=$this->getConfigValidation($id);
        
        $validator = Validator::make($request->all(),$validationconfig,$messages);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{

            $nopembayaran=$this->getNoPembayaran();

            $tagihan = Tagihan::find($id);
            $data=[
    
                'no_pembayaran'=>$nopembayaran,
                'tanggal_pembayaran'=>date('Y-m-d')
            ];
            
            //insert tabel pembayaran
            $pembayaran=$tagihan->pembayaran()->create($data);

            
            $Pembayaran = Pembayaran::find($pembayaran->id);

            foreach($tagihan->detailtagihan as $dtagihan){
    
                $detailtagihan = Detailtagihan::find($dtagihan->id);
    
                $data=[
                    'nama_tagihan'=>$dtagihan->nama_tagihan,
                    'nominal'=>$request[$dtagihan->nama_tagihan]
                ];
                $pembayaran->detailpembayaran()->create($data);
            }

            //jika lunas

            $lunas=$this->cekLunasTagihan($id);
            if($lunas==true){
                $data=[

                    'status'=>'Lunas'

                ];

                $tagihan = Tagihan::find($id);
                $tagihan->status='Lunas';
                $tagihan->save();



            }

            $message = [
                'alert' => 'success',
                'title' => 'Pembayaran Berhasil'
            ];
            
            return redirect('pembayaran')->with('message', $message);

            
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['tagihan'] = Santri::find($id);
        return view('tagihan.edit',$data);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    private function getSettingTagihan(){

        //mendapatkan config tagihan
        $settingtagihan = Settingtagihan::all();

        $datasettingtagihan=[];

        foreach($settingtagihan as $value){

            $data=[

                'nama_tagihan'=>$value->nama_tagihan,
                'nominal'=>$value->nominal
            ];

            array_push($datasettingtagihan,$data);

        }

        return $datasettingtagihan;

    }
    private function getConfigValidation($id){


        $data=$this->getSisaTagihan($id);


        $validation=[];

        foreach($data as $value){


            $array=[

                $value['nama_tagihan']=>'required|numeric|min:0|max:'.$value['sisa']
            ];

            $validation=array_merge($validation,$array);
        }

        
        return $validation;
    }
    private function getNoPembayaran(){

        $no='200';
        $no .=date('Y').date('m');

        $pembayaran = Pembayaran::where('no_pembayaran','like',$no.'%')->count();

        if($pembayaran!=0){

            $pembayaran++;
            if(strlen($pembayaran)==1){

                $no .='0000';
                $no .=$pembayaran;

            }elseif(strlen($pembayaran)==2){

                $no .='000';
                $no .=$pembayaran;
            }elseif(strlen($pembayaran)==3){

                $no .='00';
                $no .=$pembayaran;
            }elseif(strlen($pembayaran)==4){

                $no .='0';
                $no .=$pembayaran;
            }elseif(strlen($pembayaran)==5){

                $no .=$pembayaran;
            }else{

                $no=false;
            }

        }else{

            $no .='00001';

        }

        return $no;
    }
    private function getDataDetailTagihan($id){

        $tagihan = Tagihan::with(['detailtagihan','detailpembayaran'])->find($id);
        $detailtagihan=[];

        //mencari sisa
        $sisa=0;
        foreach($tagihan->detailtagihan as $dtagihan){
            $sisa=$dtagihan->nominal;
            foreach($tagihan->detailpembayaran as $dpembayaran){

                if($dtagihan->nama_tagihan==$dpembayaran->nama_tagihan){

                   $sisa=$sisa-$dpembayaran->nominal;
                    
                }
            }
            //menulis ulang array
            $array=[

                'nama_tagihan'=>$dtagihan->nama_tagihan,
                'total_tagihan'=>$dtagihan->nominal,
                'sisa'=>$sisa,

            ];

            array_push($detailtagihan,$array);
        }

        $data['tagihanid']=$tagihan->id;
        $data['detailtagihan']=$detailtagihan;

        return $data;
    }
    private function getSisaTagihan($id){

        $tagihan = Tagihan::with(['detailtagihan','detailpembayaran','pembayaran'])->find($id);
        $detailtagihan=[];

        //mencari sisa
        $sisa=0;
        foreach($tagihan->detailtagihan as $dtagihan){
            $sisa=$dtagihan->nominal;
            foreach($tagihan->detailpembayaran as $dpembayaran){

                if($dtagihan->nama_tagihan==$dpembayaran->nama_tagihan){

                   $sisa=$sisa-$dpembayaran->nominal;
                    
                }
            }
            //menulis ulang array
            $array=[

                'nama_tagihan'=>$dtagihan->nama_tagihan,
                'total_tagihan'=>$dtagihan->nominal,
                'sisa'=>$sisa,

            ];

            array_push($detailtagihan,$array);
        }

        return $detailtagihan;

    }
    private function cekLunasTagihan($id){

        $data=$this->getSisaTagihan($id);
        
        $lunas=false;
        
        foreach($data as $value){

            if($value['sisa']==0){

                $lunas=true;

            }else{

                $lunas=false;
            }

        }

        if($lunas==true){

            return true;

        }else{

            return false;

        }

    }
}
