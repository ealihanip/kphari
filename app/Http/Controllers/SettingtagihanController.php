<?php

namespace App\Http\Controllers;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Models\Settingtagihan;
use App\Http\Requests\SettingTagihanRequest;


class SettingtagihanController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('settingtagihan.index');
    }

    public function getdata()
    {
        $data = Settingtagihan::all();

        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '
                <a href="settingtagihan/edit/'.$data->id.'" class="btn btn-xs btn-primary"><i ></i> Edit</a>
                <a href="settingtagihan/destroy/'.$data->id.'" class="btn btn-xs btn-danger"><i ></i> Hapus</a>
                ';
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('settingtagihan.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SettingTagihanRequest $request)
    {
           
        $data = [
            'nama_tagihan' => $request->namatagihan,
            'nominal' => $request->nominal,
            
        ];
        
        $store = Settingtagihan::insert($data);
        
        
        $message = [
            'alert' => 'success',
            'title' => 'Data Berhasil Di Simpan'
        ];
        
        return redirect()->back()->with('message', $message);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['settingtagihan'] = Settingtagihan::find($id);
        return view('settingtagihan.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SettingtagihanRequest $request)
    {


        $data = [
            'nama_tagihan' => $request->namatagihan,
            'nominal' => $request->nominal,
            
        ];

        $store = Settingtagihan::where('id', $request->id)->update($data);

        $message = [
            'alert' => 'success',
            'title' => 'Data Berhasil Di Update'
        ];
        
        return redirect()->back()->with('message', $message);
               
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $store = Settingtagihan::where('id', $id)->delete();
        return redirect('settingtagihan');
    }


    
}
