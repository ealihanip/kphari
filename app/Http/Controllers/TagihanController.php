<?php

namespace App\Http\Controllers;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Models\Tagihan;
use App\Models\Settingtagihan;
use App\Models\Santri;
use App\Models\Detailtagihan;
use App\Http\Requests\TagihanRequest;


class TagihanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('tagihan.index');
    }

    public function getdata()
    {
        $data = Tagihan::with(['santri'])->get();
        
        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '
                <a href="tagihan/show/'.$data->id.'" class="btn btn-xs btn-warning"><i ></i> Detail</a>
                ';
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('tagihan.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagihanRequest $request)
    {
        
        //cek data apa sudah ada atau belum

        $cekdatatelahada=$this->cekDataTersedia($request->bulan,$request->tahun);

        

        if($cekdatatelahada==true){

            $message = [
                'alert' => 'error',
                'title' => 'Data Tagihan Telah Ada'
            ];
            
            return redirect()->back()->with('message', $message);

        }else{

            $santri = Santri::all();
            $nourut=0;

            foreach ($santri as $value){

                //membuat No Tagihan

                $notagihan=$this->getNo('100',$request->bulan,$request->tahun,$nourut);
                $nourut++;
                
                //end membuat No Tagihan
                
                
                
                $santri = Santri::find($value->id);

                $data = [
                    'no_tagihan' => $notagihan,
                    'bulan' => $request->bulan,
                    'tahun' => $request->tahun
                ];

                $tagihan=$santri->tagihan()->create($data);

                $tagihan = Tagihan::find($tagihan->id);

                //mengambil dari setting
                $data = $this->getSettingTagihan();
                
                $detailtagihan=$tagihan->detailtagihan()->createMany($data);

                
            }


            $message = [
                'alert' => 'Succes',
                'title' => 'Data Tagihan Telah dibuat'
            ];
            
            return redirect()->back()->with('message', $message);

            
        }
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['tagihan'] = Tagihan::with(['santri','detailtagihan','detailpembayaran'])->find($id);
        return view('tagihan.show',$data);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['tagihan'] = Santri::find($id);
        return view('tagihan.edit',$data);
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   

    function getSettingTagihan(){

        //mendapatkan config tagihan
        $settingtagihan = Settingtagihan::all();

        $datasettingtagihan=[];
        
        foreach($settingtagihan as $value){

            $data=[

                'nama_tagihan'=>$value->nama_tagihan,
                'nominal'=>$value->nominal
            ];

            array_push($datasettingtagihan,$data);

        }

        return $datasettingtagihan;

    }

    function getNo($kode,$bulan,$tahun,$nourut){

        $no='100';

        //100 ditambahtahun
        $no .=$tahun;

        //jika bulan 1 digit
        if(strlen($bulan)==1){

            $no .='0';
            $no .=$bulan;

        }else{
            
            $no .=$bulan;

        }

        if($nourut!=0){

            $nourut++;
            if(strlen($nourut)==1){

                $no .='0000';
                $no .=$nourut;

            }elseif(strlen($nourut)==2){

                $no .='000';
                $no .=$nourut;
            }elseif(strlen($nourut)==3){

                $no .='00';
                $no .=$nourut;
            }elseif(strlen($nourut)==4){

                $no .='0';
                $no .=$nourut;
            }elseif(strlen($nourut)==5){

                $no .=$nourut;
            }else{

                $no=false;
            }

        }else{

            $no .='00001';

        }

        return $no;
    }

    function cekDataTersedia($bulan,$tahun){

        $data = Tagihan::where([
            ['bulan', '=', $bulan],
            ['tahun', '=', $tahun]]
        )
        ->count();
        
        if($data!=0){

            return true;

        }else{

            return false;
        }

    }

}
