<?php

namespace App\Http\Controllers;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Models\Santri;
use App\Http\Requests\SantriStoreRequest;
use App\Http\Requests\SantriUpdateRequest;

class SantriController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('santri.index');
    }

    public function getdata()
    {
        $data = Santri::where('deleted', false);
        
       


        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '
                <a href="santri/show/'.$data->id.'" class="btn btn-xs btn-warning"><i ></i> Detail</a>
                <a href="santri/edit/'.$data->id.'" class="btn btn-xs btn-primary"><i ></i> Edit</a>
                <a href="santri/destroy/'.$data->id.'" class="btn btn-xs btn-danger"><i ></i> Hapus</a>
                ';
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('santri.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SantriStoreRequest $request)
    {
           
        $data = [
            'nis' => $request->nis,
            'angkatan' => $request->angkatan,
            'nama_santri' => $request->nama_santri,
            'jenis_kelamin' => $request->jenis_kelamin,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'alamat' => $request->alamat,
            'nama_orang_tua' => $request->nama_orang_tua,
            'no_hp_orang_tua' => $request->no_hp_orang_tua,
            'foto' => '',
        ];
        //mendapatkan id dari data yang baru di input 
        $id = Santri::insertGetId($data);
        
        //upload image
        $path='images/';
        $path .=$id;
        $getimageName = 'foto'.'.'.$request->foto->getClientOriginalExtension();
        $request->foto->move(public_path($path), $getimageName);
        //end upload image

        //reques foto dengan nilai baru
        $request->foto=$path.'/'.$getimageName;

        $data = [
            'foto' => $request->foto,
        ];

        
        $store = Santri::where('id', $id)->update($data);
        
        
        $message = [
            'alert' => 'success',
            'title' => 'Data Berhasil Di Simpan'
        ];
        
        return redirect()->back()->with('message', $message);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $data['santri'] = Santri::find($id);
        return view('santri.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['santri'] = Santri::find($id);
        return view('santri.edit',$data);
    }

    /** k
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SantriUpdateRequest $request)
    {

        //jika gambar diganti
        if($request->foto!=null){

            $id=$request->idsantri;
            
            //upload image
            $path='images/';
            $path .=$request->nis;
            $getimageName = 'foto'.'.'.$request->foto->getClientOriginalExtension();
            $request->foto->move(public_path($path), $getimageName);
            //end upload image

            //reques foto dengan nilai baru
            $request->foto=$path.'/'.$getimageName;
            
            
            $data = [
                'nis' => $request->nis,
                'angkatan' => $request->angkatan,
                'nama_santri' => $request->nama_santri,
                'jenis_kelamin' => $request->jenis_kelamin,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
                'alamat' => $request->alamat,
                'nama_orang_tua' => $request->nama_orang_tua,
                'no_hp_orang_tua' => $request->no_hp_orang_tua,
                'foto' => $request->foto,
            ];

            
            $store = Santri::where('id', $id)->update($data);

            $message = [
                'alert' => 'success',
                'title' => 'Data Berhasil Di Update'
            ];
            
            return redirect()->back()->with('message', $message);

        }else{//jika gambar tidak diganti
            
            $id=$request->idsantri;
            
            $data = [
                'nis' => $request->nis,
                'angkatan' => $request->angkatan,
                'nama_santri' => $request->nama_santri,
                'jenis_kelamin' => $request->jenis_kelamin,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
                'alamat' => $request->alamat,
                'nama_orang_tua' => $request->nama_orang_tua,
                'no_hp_orang_tua' => $request->no_hp_orang_tua,
                // 'foto' => $request->foto,
            ];

            
            $store = Santri::where('id', $id)->update($data);

            $message = [
                'alert' => 'success',
                'title' => 'Data Berhasil Di Update'
            ];
            
            return redirect()->back()->with('message', $message);
        }        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $data = [
            'deleted' => true,
        ];

        $store = Santri::where('id', $id)->update($data);
        return redirect('santri');
    }
}
