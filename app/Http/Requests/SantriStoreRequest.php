<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SantriStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nis' => 'required|numeric|unique:santri,nis|digits:10',
            'angkatan' => 'required',
            'nama_santri' => 'required|min:3|max:30',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'alamat' => 'required',
            'nama_orang_tua' => 'required',
            'no_hp_orang_tua' => 'required',
            'foto' => 'required|image'
        ];
    }

    public function messages()
    {
        return [
            'nis.required' => 'NIS Tidak Boleh Kosong',
            'nis.unique' => 'NIS Sudah Terdaftar',
            'nis.numeric' => 'NIS harus angka',
            'nis.digits' => 'NIS harus 10 karakter',
            'angkatan.required' => 'Angkatan harus di isi',
            'nama_santri.required' => 'Nama Santri harus di isi',
            'nama_santri.min' => 'Nama Santri harus lebih dari 3 huruf',
            'jenis_kelamin.required' => 'Harus di isi',
            'tempat_lahir.required' => 'Harus di isi',
            'tanggal_lahir.required' => 'Harus di isi',
            'alamat.required' => 'Harus lengkap',
            'nama_orang_tua.required' => 'Harus di isi',
            'no_hp_orang_tua.required' => 'Harus di isi',
            'foto.required' => 'Harus di isi',
            'foto.image' => 'Harus Gambar'
        ];
    }
}
