<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Pembayaran extends Model
{
    public $timestamps = false;

    protected $table="pembayaran";
    protected $primaryKey="id";
    protected $fillable=[
        'no_pembayaran',
        'tanggal_pembayaran'
    ];

    public function detailpembayaran()
    {
        return $this->hasMany('App\Models\Detailpembayaran');
    }
    
    public function tagihan()
    {
        return $this->belongsTo('App\Models\Tagihan'::class);
    }

    public function santris()
    {
        return $this->tagihan->santri();
    }

    
}


