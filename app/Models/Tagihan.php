<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Tagihan extends Model
{
    public $timestamps = false;

    protected $table="tagihan";
    protected $primaryKey="id";
    protected $fillable=[
        'no_tagihan',
        'bulan',
        'tahun',
        'status'
    ];

    public function santri()
    {
        return $this->belongsTo('App\Models\Santri');
    }

    public function detailtagihan()
    {
        return $this->hasMany('App\Models\Detailtagihan');
    }

    public function pembayaran()
    {
        return $this->hasMany('App\Models\Pembayaran');
    }

    public function detailpembayaran()
    {
        return $this->hasManyThrough('App\Models\Detailpembayaran', 'App\Models\Pembayaran');
    }



}
