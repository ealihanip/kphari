<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Santri extends Model
{
    public $timestamps = false;

    protected $table="santri";
    protected $primaryKey="id";
    public $incrementing = false;


    public function tagihan()
    {
        return $this->hasMany('App\Models\Tagihan');
    }

    public function detailtagihan()
    {
        return $this->hasManyThrough('App\Models\Detailtagihan', 'App\Models\Tagihan');
    }
}
