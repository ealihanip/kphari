<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Detailtagihan extends Model
{
    public $timestamps = false;

    protected $table="detailtagihan";
    protected $primaryKey="id";
    protected $fillable=[
        'nama_tagihan',
        'nominal'
    ];

    public function tagihan()
    {
        return $this->belongsTo('App\Models\Tagihan');
    }


}
