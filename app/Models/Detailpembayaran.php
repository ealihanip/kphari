<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Detailpembayaran extends Model
{
    public $timestamps = false;

    protected $table="detailpembayaran";
    protected $primaryKey="id";
    protected $fillable=[
        'nama_tagihan',
        'nominal'
    ];

    public function detailtagihan()
    {
        return $this->belongsTo('App\Models\Detailtagihan');
    }
    public function pembayaran()
    {
        return $this->belongsTo('App\Models\Pembayaran');
    }

}
