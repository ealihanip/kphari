<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailpembayaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detailpembayaran', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pembayaran_id');
            $table->string('nama_tagihan',15);
            $table->integer('nominal');
            $table->foreign('pembayaran_id')->references('id')->on('pembayaran');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detailpembayaran');
    }
}
