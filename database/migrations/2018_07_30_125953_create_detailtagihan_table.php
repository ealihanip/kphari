<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailtagihanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detailtagihan', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tagihan_id');
            $table->string('nama_tagihan',15);
            $table->integer('nominal');
            $table->foreign('tagihan_id')->references('id')->on('tagihan');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detailtagihan');
    }
}
