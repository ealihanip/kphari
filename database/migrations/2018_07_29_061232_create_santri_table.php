<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSantriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('santri', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nis',15);
            $table->string('angkatan',4);
            $table->string('nama_santri',30);
            $table->string('jenis_kelamin',10);
            $table->string('tempat_lahir',15);
            $table->date('tanggal_lahir');
            $table->text('alamat');	
            $table->string('nama_orang_tua',30);
            $table->string('no_hp_orang_tua',15);
            $table->string('foto',30);
            $table->tinyInteger('deleted')->default(false);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('santri');
    }
}
