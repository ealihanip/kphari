<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/guru',[
//     'as' => 'guru.index',
//     'uses' => 'GuruController@index'
// ]);
// Route::get('/guru/create',[
//     'as' => 'guru.create',
//     'uses' => 'GuruController@create'
// ]);
// Route::post('/guru/store',[
//     'as' => 'guru.store',
//     'uses' => 'GuruController@store'
// ]);
// Route::get('/guru/show/{id}',[
//     'as' => 'guru.show',
//     'uses' => 'GuruController@show'
// ]);
// Route::get('/guru/edit/{id}',[
//     'as' => 'guru.edit',
//     'uses' => 'GuruController@edit'
// ]);
// Route::post('/guru/updata',[
//     'as' => 'guru.update',
//     'uses' => 'GuruController@update'
// ]);
// Route::get('/guru/destroy/{id}',[
//     'as' => 'guru.destroy',
//     'uses' => 'GuruController@destroy'
// ]);
// Route::get('/guru/getdata',[
//     'as' => 'guru.getdata',
//     'uses' => 'GuruController@getdata'
// ]);


    
    //routes santri

    Route::get('/santri',[
        'as' => 'santri.index',
        'uses' => 'santriController@index'
    ]);
    Route::get('/santri/create',[
        'as' => 'santri.create',
        'uses' => 'santriController@create'
    ]);
    Route::post('/santri/store',[
        'as' => 'santri.store',
        'uses' => 'santriController@store'
    ]);
    Route::get('/santri/show/{id}',[
        'as' => 'santri.show',
        'uses' => 'santriController@show'
    ]);
    Route::get('/santri/edit/{id}',[
        'as' => 'santri.edit',
        'uses' => 'santriController@edit'
    ]);
    Route::post('/santri/updata',[
        'as' => 'santri.update',
        'uses' => 'santriController@update'
    ]);
    Route::get('/santri/destroy/{id}',[
        'as' => 'santri.destroy',
        'uses' => 'santriController@destroy'
    ]);
    Route::get('/santri/getdata',[
        'as' => 'santri.getdata',
        'uses' => 'santriController@getdata'
    ]);



    //routes tagihan

    Route::get('/tagihan',[
        'as' => 'tagihan.index',
        'uses' => 'tagihanController@index'
    ]);
    Route::get('/tagihan/create',[
        'as' => 'tagihan.create',
        'uses' => 'tagihanController@create'
    ]);
    Route::post('/tagihan/store',[
        'as' => 'tagihan.store',
        'uses' => 'tagihanController@store'
    ]);
    Route::get('/tagihan/show/{id}',[
        'as' => 'tagihan.show',
        'uses' => 'tagihanController@show'
    ]);
    Route::get('/tagihan/edit/{id}',[
        'as' => 'tagihan.edit',
        'uses' => 'tagihanController@edit'
    ]);
    Route::post('/tagihan/updata',[
        'as' => 'tagihan.update',
        'uses' => 'tagihanController@update'
    ]);
    Route::get('/tagihan/destroy/{id}',[
        'as' => 'tagihan.destroy',
        'uses' => 'tagihanController@destroy'
    ]);
    Route::get('/tagihan/getdata',[
        'as' => 'tagihan.getdata',
        'uses' => 'tagihanController@getdata'
    ]);


    //routes settingtagihan

    Route::get('/settingtagihan',[
        'as' => 'settingtagihan.index',
        'uses' => 'settingtagihanController@index'
    ]);
    Route::get('/settingtagihan/create',[
        'as' => 'settingtagihan.create',
        'uses' => 'settingtagihanController@create'
    ]);
    Route::post('/settingtagihan/store',[
        'as' => 'settingtagihan.store',
        'uses' => 'settingtagihanController@store'
    ]);
    Route::get('/settingtagihan/show/{id}',[
        'as' => 'settingtagihan.show',
        'uses' => 'settingtagihanController@show'
    ]);
    Route::get('/settingtagihan/edit/{id}',[
        'as' => 'settingtagihan.edit',
        'uses' => 'settingtagihanController@edit'
    ]);
    Route::post('/settingtagihan/updata',[
        'as' => 'settingtagihan.update',
        'uses' => 'settingtagihanController@update'
    ]);
    Route::get('/settingtagihan/destroy/{id}',[
        'as' => 'settingtagihan.destroy',
        'uses' => 'settingtagihanController@destroy'
    ]);
    Route::get('/settingtagihan/getdata',[
        'as' => 'settingtagihan.getdata',
        'uses' => 'settingtagihanController@getdata'
        
    ]);

    //routes pembayaran

    Route::get('/pembayaran',[
        'as' => 'pembayaran.index',
        'uses' => 'pembayaranController@index'
    ]);
    Route::get('/pembayaran/create/{id}',[
        'as' => 'pembayaran.create',
        'uses' => 'pembayaranController@create'
    ]);
    Route::post('/pembayaran/store',[
        'as' => 'pembayaran.store',
        'uses' => 'pembayaranController@store'
    ]);
    Route::get('/pembayaran/show/{id}',[
        'as' => 'pembayaran.show',
        'uses' => 'pembayaranController@show'
    ]);
    Route::get('/pembayaran/edit/{id}',[
        'as' => 'pembayaran.edit',
        'uses' => 'pembayaranController@edit'
    ]);
    Route::post('/pembayaran/updata',[
        'as' => 'pembayaran.update',
        'uses' => 'pembayaranController@update'
    ]);
    Route::get('/pembayaran/destroy/{id}',[
        'as' => 'pembayaran.destroy',
        'uses' => 'pembayaranController@destroy'
    ]);
    Route::get('/pembayaran/getdatatagihan',[
        'as' => 'pembayaran.getdatatagihan',
        'uses' => 'pembayaranController@getdatatagihan'
        
    ]);
    Route::get('/pembayaran/getdatapembayaran',[
        'as' => 'pembayaran.getdatapembayaran',
        'uses' => 'pembayaranController@getdatapembayaran'
        
    ]);

    Route::get('/pembayaran/historipembayaran',[
        'as' => 'pembayaran.historipembayaran',
        'uses' => 'pembayaranController@historipembayaran'
        
    ]);


    Auth::routes();
    Route::get('/', 'SantriController@index');
